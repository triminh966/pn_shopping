'use strict';

// Declare app level module which depends on views, and components
angular.module('pnApp', ['ngRoute'])
  .config(configRoutes)

  configRoutes.$inject = [ '$routeProvider' ];

  function configRoutes($routeProvider) {
    $routeProvider
    .when('/', {
      templateUrl : '/frontend/index.html',
    }).otherwise({
      redirectTo : '/'
    });
  }
;
